package UnoDatenbank;

public class HighScore
{

	private String	Nickname;
	private int		SessionID;
	private int		Punkte;
	private String	Datum;

	public String getNickname()
	{
		return Nickname;
	}

	@Override
	public String toString()
	{
		return "HighScore [Nickname=" + Nickname + ", SessionID=" + SessionID + ", Punkte=" + Punkte + ", Datum="
				+ Datum + ", getNickname()=" + getNickname() + ", getSessionID()=" + getSessionID() + ", getPunkte()="
				+ getPunkte() + ", getDatum()=" + getDatum() + ", getClass()=" + getClass() + ", hashCode()="
				+ hashCode() + ", toString()=" + super.toString() + "]";
	}

	public void setNickname(String nickname)
	{
		Nickname = nickname;
	}

	public int getSessionID()
	{
		return SessionID;
	}

	public void setSessionID(int sessionID)
	{
		SessionID = sessionID;
	}

	public int getPunkte()
	{
		return Punkte;
	}

	public void setPunkte(int punkte)
	{
		Punkte = punkte;
	}

	public String getDatum()
	{
		return Datum;
	}

	public void setDatum(String datum)
	{
		Datum = datum;
	}

}