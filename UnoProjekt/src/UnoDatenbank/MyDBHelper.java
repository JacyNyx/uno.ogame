package UnoDatenbank;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MyDBHelper
{
	private Connection con = null;

		public void init()
		{
			try
			{
				// 1a. Jar-Files referenzieren - Java Build Path
				// 1b. Load Access Driver
				Class.forName("net.ucanaccess.jdbc.UcanaccessDriver");
			} catch (ClassNotFoundException e)
			{

				e.printStackTrace();
			}

			try
			{
				// 2. get Connection to database
				con = DriverManager.getConnection("jdbc:ucanaccess://" + "U:\\DBD2018\\HighscoreVerwaltung.accdb");

				System.out.println("SUPER - hat funktioniert :-) ");

			} catch (SQLException e)
			{
				e.printStackTrace();
			}

		}

		public void close()
		{
			if (con != null)
				try
				{
					con.close();
				} catch (SQLException e)
				{
					e.printStackTrace();
					System.out.println("Fehler beim Schliessen der Verbindung");
				}
		}

		public void startSession(String nickname, int sessionID)
		{
			String insSession = "Insert into highscore(nickname,sessionid,punkte,datum) ";
			insSession += " values(?,?,0,?)";

			try
			{
				PreparedStatement stmt = con.prepareStatement(insSession);
				stmt.setString(1, nickname);
				Date date = new Date();
				DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
				stmt.setInt(2, sessionID);
				stmt.setString(3, dateFormat.format(date));

				stmt.executeUpdate();

			} catch (SQLException e)
			{

				e.printStackTrace();
			}

		}

		public void updateSession(String nickname, int sessionID, int punkte)
		{
			String updSession = "update highscore set punkte = punkte + ? ";
			updSession += " where nickname=? and sessionid=?";

			try
			{
				PreparedStatement stmt = con.prepareStatement(updSession);
				stmt.setInt(1, punkte);
				stmt.setString(2, nickname);
				stmt.setInt(3, sessionID);

				stmt.executeUpdate();

			} catch (SQLException e)
			{

				e.printStackTrace();
			}

		}

		public ArrayList<HighScore> getHistorieForNickname(String nickname)
		{
			ArrayList<HighScore> result = new ArrayList<HighScore>();

			String select = "Select Nickname, SessionID, PUnkte, Datum ";
			select += " from Highscore where nickname=?";
			try
			{
				PreparedStatement stmt = con.prepareStatement(select);
				stmt.setString(1, nickname);
				ResultSet rs = stmt.executeQuery();

				while (rs.next())
				{
					HighScore h = new HighScore();
					h.setNickname(nickname);
					h.setSessionID(rs.getInt(2));
					h.setPunkte(rs.getInt(3));
					h.setDatum(rs.getString(4));

					result.add(h);
				}

			} catch (SQLException e)
			{

				e.printStackTrace();
			}
			return result;
		}

		public ArrayList<HighScore> getHistorieForSession(int sessionID)
		{
			return null;
		}

		public String getLoser()
		{
			return null;
		}

		public String getWinner()
		{
			String winner = "";
			try
			{
				String selectWinner = "select nickname, sum(punkte)/count(*) from highscore ";
				selectWinner += " group by nickname ";
				selectWinner += " order by sum(punkte)/count(*) asc";
				PreparedStatement stmt = con.prepareStatement(selectWinner);
				ResultSet rs = stmt.executeQuery();

				rs.next();

				winner = rs.getString(1) + " - " + rs.getInt(2);

			} catch (Exception e)
			{
				// TODO: handle exception
			}

			return winner;
		}

	}

