package UnoDatenbank;

import java.util.ArrayList;
import java.util.Scanner;

import UnoGame.Card;

public class Player {

	private String Nickname;
	private ArrayList<Card> myCards = new ArrayList<>();
	private int pointsPerRound;
	private int totalPoints;
		
	

	public void setMyCardsOnEmpty() {
		myCards.clear();
	}

	public void setName(String name) {
		this.Nickname = name;
	}

	public int getPointsPerRound() {
		return pointsPerRound;
	}

	public void setPointsPerRound(int pointsPerRound) {
		this.pointsPerRound = pointsPerRound;
	}

	public int getTotalPoints() {
		return totalPoints;
	}

	public void setTotalPoints(int totalPoints) {
		this.totalPoints = totalPoints;
	}

	public Player() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Player(String name) {
		super();
		this.Nickname = name;
	}

	public String getName() {
		return Nickname;
	}

	public void takeCard(Card card) {
		myCards.add(card);
	}

	public boolean hasCard() {
		if (myCards.size() > 0) {
			return true;
		}
		return false;
	}

	public void removeCard(Card card) {

		myCards.remove(card);
	}

	public ArrayList<Card> getCards() {

		return myCards;
	}

	public int howManyCardsLeft() {
		return myCards.size();
	}

	public String sayUno() {
		return "UNO";
	}

	public Card selectCard(Card currentStackCard) {

		Card cardToBePlayed = null;
		Scanner sc = new Scanner(System.in);

		while (cardToBePlayed == null) {

			System.out.println("Please select the color of the card you'd like to play (red, green, blue, yellow): ");
			String inputColor = sc.next();
			System.out.println("Please select the value of the card you'd like to play: ");
			int inputValue = sc.nextInt();

			for (Card card : myCards) {
				if (inputColor.equals(card.getColor()) && inputValue == card.getValue()) {
					cardToBePlayed = card;
				}
			}
		}

		return cardToBePlayed;

	}

	// Spieler entscheidet, ob er eine Karte spielen oder vom Stack abheben will
	public String selectAction(Card currentStackCard) {
		
		Scanner sc = new Scanner(System.in);
		String selection = null;
		boolean validSelection = false;
		while (validSelection == false) {

			System.out
					.println("Select if you want to play a card or take a card from the deck. Please type 's' or 't' ");
			selection = sc.next();
			

			if (selection.equals("s")) {
				validSelection = true;
			} else if (selection.equals("t")) {
				validSelection = true;
			} else {
				System.out.println("Invalid selection. Try again.");
			}

		}

		return selection;
	}



	

}
