package UnoServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.Date;

public class Server
{

	public static void main(String[] args) throws IOException
	{

		ServerSocket server = new ServerSocket(1111);
		server.setSoTimeout(500);
		ArrayList<Socket> players = new ArrayList<>();
		
		Date start = null;

		while (players.size() < 5)
		{
			try
			{
				Socket socket = server.accept();
				if (players.size() == 0)
				{
					start = new Date();
				}
				players.add(socket);
			} catch (SocketTimeoutException e)
			{
				if (start != null)
				{
					Date current = new Date();
					if(current.getTime() - start.getTime() > 10000)
					{
						System.out.println("F�ge Bot hinzu");
					}
				}
			}

		}
server.close();
	}
}
