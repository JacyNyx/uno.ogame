package UnoClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import UnoDatenbank.Player;


public class Client
{
	public static void main(String[] args)
	{
		try (Socket socket = new Socket("172.32.0.85", 1111);
				ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
				ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());

				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));)
		{
			System.out.println("Spielernamen eingeben:");
			String name = br.readLine();
			Player p = new Player(name);
			oos.writeObject(p);
			oos.flush();

			
			@SuppressWarnings("unchecked")
			Map<String, Integer> scores = (Map<String, Integer>) ois.readObject();

			int maxValue = 0;
			String winner = "";
			List<String> winners = new ArrayList<>();

			System.out.println();
			for (Entry<String, Integer> score : scores.entrySet())
			{
				if (score.getValue() > maxValue)
				{
					maxValue = score.getValue();
				}
				System.out.println("Spieler: " + score.getKey() + " hat " + score.getValue() + " Punkte");
			}

			for (Entry<String, Integer> score : scores.entrySet())
			{
				if (score.getValue() == maxValue)
				{
					winner = score.getKey();
					winners.add(winner);
				}
			}
			System.out.println();
			
			if (winners.size() == 1)
			{
				System.out.println("Gratulation " + winners.get(0) + ", du hast mit " + maxValue + " Punkte gewonnen!");
			}
			else if (winners.size() > 1)
			{
				System.out.println("Gewonnen haben: ");
				
				for (String w : winners)
				{
					System.out.println(w);
				}
			}

		} catch (UnknownHostException e)
		{
			e.printStackTrace();
		} catch (IOException e)
		{
			e.printStackTrace();
		} catch (ClassNotFoundException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
