package uiUno;

public class CardDisplay
{
	String[][]	board;
	String		border	= "*";
	String		leer	= " ";
	
	int	bl;	// boardlength
	int	bw;	// boardwidth
	int	cs;	//columnsize
	int	rs;	//rowsize

	
//	formt die R�nder der Karten
	public CardDisplay(String color, String value)
	{
		
		bl = 7;
		bw = 5;
		
		board = new String[bl][bw];
		for (int row = 0; row < board.length; row++)
		{
			for (int column = 0; column < board[0].length; column++)
			{
				if (row == 0 || row == bl - 1)
				{
					board[row][column] = border;
				}

				else if (column == 0 || column == bw -1)
				{
					board[row][column] = border;
				}

				else
				{
					board[row][column] = leer;
				}
			}
		}

//		plaziert die Farbe der Karte mit dem ersten Buchstaben im Array
		cs = bl / 2-2;
		rs = bw/2;
		board[cs][rs] = color;
		
//		plaziert den Wert der Karte im Array
		cs = bl / 2 +1;
		rs = bw /2;
		board[cs][rs] = value;
	}

	@Override
	public String toString()
	{
		for (int row = 0; row < board.length; row++) // loop through columns
		{
			for (int column = 0; column < board[row].length; column++) // loop through column in row
			{
				System.out.print(board[row][column] + " ");
			}
			System.out.println();
		}
		return leer;
	}
}