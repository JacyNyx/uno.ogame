package GuiUno;
import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JRadioButton;
import java.awt.Window.Type;
import javax.swing.JLabel;

public class Farbauswahl
{

	private JFrame frmFarbauswahl;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Farbauswahl window = new Farbauswahl();
					window.frmFarbauswahl.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Farbauswahl()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmFarbauswahl = new JFrame();
		frmFarbauswahl.setType(Type.POPUP);
		frmFarbauswahl.setTitle("Farbauswahl");
		frmFarbauswahl.getContentPane().setBackground(new Color(135, 206, 235));
		frmFarbauswahl.setBounds(100, 100, 260, 188);
		frmFarbauswahl.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmFarbauswahl.getContentPane().setLayout(null);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Blau");
		rdbtnNewRadioButton.setBackground(new Color(0, 191, 255));
		rdbtnNewRadioButton.setBounds(140, 51, 76, 35);
		frmFarbauswahl.getContentPane().add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnGrn = new JRadioButton("Gr\u00FCn");
		rdbtnGrn.setBackground(new Color(60, 179, 113));
		rdbtnGrn.setBounds(34, 51, 76, 35);
		frmFarbauswahl.getContentPane().add(rdbtnGrn);
		
		JRadioButton rdbtnRot = new JRadioButton("Rot");
		rdbtnRot.setBackground(new Color(220, 20, 60));
		rdbtnRot.setBounds(140, 94, 76, 35);
		frmFarbauswahl.getContentPane().add(rdbtnRot);
		
		JRadioButton rdbtnGelb = new JRadioButton("Gelb");
		rdbtnGelb.setBackground(new Color(255, 255, 0));
		rdbtnGelb.setBounds(34, 94, 76, 35);
		frmFarbauswahl.getContentPane().add(rdbtnGelb);
		
		JLabel lblNewLabel = new JLabel("  w\u00E4hle eine Farbe");
		lblNewLabel.setFont(new Font("Arial Rounded MT Bold", Font.ITALIC, 13));
		lblNewLabel.setBounds(56, 11, 125, 35);
		frmFarbauswahl.getContentPane().add(lblNewLabel);
	}
}
