package GuiUno;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JProgressBar;
import java.awt.Font;
import java.awt.Color;
import javax.swing.JLabel;

public class Lounge
{

	private JFrame frmLounge;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Lounge window = new Lounge();
					window.frmLounge.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Lounge()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmLounge = new JFrame();
		frmLounge.setTitle("Lounge");
		frmLounge.getContentPane().setBackground(new Color(135, 206, 235));
		frmLounge.setBounds(100, 100, 450, 188);
		frmLounge.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmLounge.getContentPane().setLayout(null);
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setBounds(36, 90, 351, 14);
		frmLounge.getContentPane().add(progressBar);
		
		JLabel lblNewLabel = new JLabel("Es wird auf weitere Spieler gewartet");
		lblNewLabel.setFont(new Font("Arial Rounded MT Bold", Font.ITALIC, 13));
		lblNewLabel.setBounds(94, 35, 247, 27);
		frmLounge.getContentPane().add(lblNewLabel);
	}
}
