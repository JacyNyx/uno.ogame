package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import UnoDatenbank.Player;
import UnoGame.Game;

public class GameTest {

	@Test
	public void testDealCards() {
		Player[] players = {new Player("1"), new Player("2"), new Player("3"), new Player("4")};
		Game g = new Game();
		g.dealCards(players);
		
		assertEquals(7, players[0].howManyCardsLeft());
	}

}
