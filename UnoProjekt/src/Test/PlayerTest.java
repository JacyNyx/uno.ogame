package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import UnoDatenbank.Player;
import UnoGame.Card;
import UnoGame.NumberCard;

public class PlayerTest {

	@Test
	public void testTakeCard() {
		
		Card c1 = new NumberCard("red",5,"number");
		Card c2 = new NumberCard("blue",5,"number");
		Card c3 = new NumberCard("yellow",9,"number");
		Player p = new Player();
		p.takeCard(c1);
		p.takeCard(c2);
		p.takeCard(c3);
		assertEquals(3,p.getCards().size());
	}

	@Test
	public void testHasCardTrue() {
		Card c1 = new NumberCard("red",5,"number");
		Card c2 = new NumberCard("blue",5,"number");
		Card c3 = new NumberCard("yellow",9,"number");
		Player p = new Player();
		p.takeCard(c1);
		p.takeCard(c2);
		p.takeCard(c3);
		assertTrue(p.hasCard());
	}
	
	@Test
	public void testHasCardFalse() {
		Player p = new Player();
		assertFalse(p.hasCard());
	}

	@Test
	public void testRemoveCard() {
		Card c1 = new NumberCard("red",5,"number");
		Card c2 = new NumberCard("blue",5,"number");
		Card c3 = new NumberCard("yellow",9,"number");
		Player p = new Player();
		p.takeCard(c1);
		p.takeCard(c2);
		p.takeCard(c3);
		p.removeCard(c2);
		assertEquals(2, p.getCards().size());
	}

	@Test
	public void testHowManyCardsLeft() {
		Card c2 = new NumberCard("blue",5,"number");
		Card c3 = new NumberCard("yellow",9,"number");
		Player p = new Player();
		p.takeCard(c2);
		p.takeCard(c3);
		assertEquals(2, p.howManyCardsLeft());
	}

}
