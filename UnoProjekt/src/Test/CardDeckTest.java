package Test;
import static org.junit.Assert.*;

import org.junit.Test;

import UnoGame.CardDeck;

public class CardDeckTest {

	@Test
	public void testCardDeckSize() {
		
		CardDeck c = new CardDeck();
		assertEquals(108,c.getCardDeck().size());
	}

	@Test
	public void testNumberCardSize() {
		CardDeck c = new CardDeck();
		assertEquals(76,c.generateNumberCardList().size());
	}
	
	@Test
	public void testActionCardSize() {
		CardDeck c = new CardDeck();
		assertEquals(24, c.generateActionCardList().size());
	}
	
	@Test
	public void testWildCardSize() {
		CardDeck c = new CardDeck();
		assertEquals(8, c.generateWildCardList().size());
	}
}
