package UnoGame;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

import UnoDatenbank.Player;

public class Game
{

	private Player[] players = new Player[4];
	CardDeck c = new CardDeck();
	private ArrayList<Card> cardDeck = c.getCardDeck(); // Kartenstapel
	private ArrayList<Card> cardStack = new ArrayList<>(); // Ablagestapel
	private Player currentPlayer;
	
	public Player getCurrentPlayer()
	{
		return currentPlayer;
	}

	public void setCurrentPlayer(Player currentPlayer)
	{
		this.currentPlayer = currentPlayer;
	}

	public void setCardStack(ArrayList<Card> cardStack)
	{
		this.cardStack = cardStack;
	}

	public void setCardDeck(ArrayList<Card> cardDeck)
	{
		this.cardDeck = cardDeck;
	}

	public Player[] getPlayers()
	{
		return players;
	}

	public ArrayList<Card> getCardDeck()
	{
		return cardDeck;
	}

	public ArrayList<Card> getCardStack()
	{
		return cardStack;
	}

	// Karten mischen - schauen, ob wirklich notwendig in dieser Form....
	public ArrayList<Card> shuffleCards(ArrayList<Card> cards)
	{

		Collections.shuffle(cards);
		return cards;
	}

	// Karten austeilen
	public void dealCards(Player[] players)
	{
		for (Player p : players)
		{
			for (int i = 0; i < 7; i++)
			{
				p.takeCard(cardDeck.remove(i));
			}
		}
	}

	// erste Karte vom Kartenstapel aufdecken
	public ArrayList<Card> putFirstCardOnCardStack()
	{

		do
		{
			cardStack.add(cardDeck.remove(0));

		} while (cardStack.get(cardStack.size() - 1).getValue() > 9);

		return cardStack;
	}

	// sind am Kartenstapel noch Karten
	public boolean cardDeckEmpty()
	{
		if (cardDeck.size() == 0)
		{
			return true;
		} else
		{
			return false;
		}
	}

	// neuen Kartenstapel aus Ablagestapel generieren
	public void newCardDeck()
	{
		if (cardDeckEmpty() == true)
		{
			Card temp = cardStack.remove(cardStack.size() - 1);
			cardDeck.addAll(cardStack);
			Collections.shuffle(cardDeck);
			cardStack.clear();
			cardStack.add(temp);
		}

	}

	// Spieler zum aktuellen Spiel hinzuf�gen
	public Player[] addPlayer(Player p)
	{

		for (int i = 0; i < players.length; i++)
		{
			if (players[i] == null)
			{
				players[i] = p;
				break;
			}

		}
		return players;
	}

	// zuf�lliger Spieler beginnt
	public Player whoStarts(Player[] players)
	{

		int i = ThreadLocalRandom.current().nextInt(0, players.length);

		return players[i];
	}

	// next player methode - gibt n�chsten spieler, der am Zug ist, zur�ck
	public Player nextPlayer()
	{
		int index = Arrays.asList(players).indexOf(currentPlayer);

		if (index == 3)
		{
			return players[0];
		} else
		{
			return players[index + 1];
		}

	}

	// nach jeder Runde checken, ob genau alle Karten noch im Umlauf sind, und keine
	// verloren gehen...
	public boolean howManyCardsAreThere(int numberOfCards)
	{

		int sum = 0;

		for (int i = 0; i < players.length; i++)
		{
			if (getPlayers()[i] != null)
			{
				sum += getPlayers()[i].getCards().size();
			}
		}

		sum += getCardDeck().size();
		sum += getCardStack().size();

		if (sum == numberOfCards)
		{
			return true;
		} else
		{
			return false;
		}
	}

	// summiert Punkte pro Spieler, wenn Gameloop zu Ende ist und ein Spieler keine
	// Karten mehr hat

	public int pointsPerRound(Player player)
	{
		int sum = 0;
		for (Card card : player.getCards())
		{
			sum += card.getValue();
		}

		return sum;
	}

	// pr�ft, ob alle Pl�tze im Player Array gef�llt sind
	public boolean playerArrayfull(Player[] p)
	{

		for (int i = 0; i < p.length; i++)
		{
			if (p[i] == null)
			{
				return false;
			}
		}

		return true;
	}

	// generiert Bots
	public void generateBots()
	{
		int counter = 1;
		for (int i = 0; i < players.length; i++)
		{
			if (players[i] == null)
			{
				String name = "Bot " + counter;
				counter++;
				addPlayer(new Bot(name));
			}
		}
	}

	// z�hlt Punkte nach jedem Durchgang zusammen, der Gewinner bekommt alle Punkte
	public int addUpPointsPerRound()
	{
		int sum = 0;
		for (int i = 0; i < players.length; i++)
		{
			for (Card c : players[i].getCards())
			{
				sum += c.getValue();
			}
		}
		return sum;
	}

	// addiert Punkte pro Runde und aktuellen Punktestand pro Spieler
	public void addUpTotalPointsForRoundWinner(Player p)
	{

		int sum = addUpPointsPerRound() + p.getTotalPoints();
		p.setTotalPoints(sum);
	}

	public boolean playerHas500PointsOrMore()
	{
		for (int i = 0; i < players.length; i++)
		{
			if (players[i].getTotalPoints() >= 500)
			{
				return true;
			}
		}

		return false;
	}

	// wer als erster 500 Punkte hat, gewinnt
	public Player whoWinsTheGame()
	{
		Player p = players[3];

		for (int i = 0; i < players.length; i++)
		{
			if (p.getTotalPoints() < players[i].getTotalPoints())
			{
				p = players[i];
			}
		}

		return p;
	}

	public void emptyPlayerHands()
	{
		for (int i = 0; i < players.length; i++)
		{
			players[i].setMyCardsOnEmpty();
		}
	}

	public void emptyCardStack()
	{
		cardStack.clear();
	}

}
