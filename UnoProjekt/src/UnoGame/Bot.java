package UnoGame;

import java.util.ArrayList;

import UnoDatenbank.Player;

public class Bot extends Player {
		
	private String name;

	public Bot(String name) {
		super();
		this.name = name;
	}
	
	
	
	public void setName(String name) {
		this.name = name;
	}





	@Override
	public void setMyCardsOnEmpty() {
		// TODO Auto-generated method stub
		super.setMyCardsOnEmpty();
	}



	@Override
	public String getName() {
		return name;
	}
	

	@Override
	public int getPointsPerRound() {
		// TODO Auto-generated method stub
		return super.getPointsPerRound();
	}



	@Override
	public void setPointsPerRound(int pointsPerRound) {
		// TODO Auto-generated method stub
		super.setPointsPerRound(pointsPerRound);
	}



	@Override
	public int getTotalPoints() {
		// TODO Auto-generated method stub
		return super.getTotalPoints();
	}



	@Override
	public void setTotalPoints(int totalPoints) {
		// TODO Auto-generated method stub
		super.setTotalPoints(totalPoints);
	}



	@Override
	public void takeCard(Card card) {
		// TODO Auto-generated method stub
		super.takeCard(card);
	}

	@Override
	public boolean hasCard() {
		// TODO Auto-generated method stub
		return super.hasCard();
	}

	@Override
	public void removeCard(Card card) {
		// TODO Auto-generated method stub
		super.removeCard(card);
	}

	@Override
	public ArrayList<Card> getCards() {
		// TODO Auto-generated method stub
		return super.getCards();
	}

	@Override
	public int howManyCardsLeft() {
		// TODO Auto-generated method stub
		return super.howManyCardsLeft();
	}

	@Override
	public String sayUno() {
		// TODO Auto-generated method stub
		return super.sayUno();
	}

	@Override
	public Card selectCard(Card currentStackCard) {
		Card selectedCard = null;
		for (Card card : getCards()) {
			if(card.match(currentStackCard)) {
				selectedCard = card;
				break;
			}
	}
		return selectedCard;
	}

	@Override
	public String selectAction(Card currentStackCard) {
		;
		for (Card card : getCards()) {
			if(card.match(currentStackCard)) {
				return "s";
			}
		}
				return "t";
			}
	}
	
		

