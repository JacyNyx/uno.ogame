package UnoGame;

import java.util.ArrayList;
import java.util.Collections;

public class Gameloop {

	public static void main(String[] args) {

		// Scanner in der main anlegen und jedem Player mit�bergeben

		Game g = new Game();

		//Player p1 = new Player("p1");
		/*
		Player p2 = new Player("p2");
		Player p3 = new Player("p3");
		Player p4 = new Player("p4");
		*/
		//g.addPlayer(p1);
		/*
		g.addPlayer(p2);
		g.addPlayer(p3);
		g.addPlayer(p4);
	*/
		if(!g.playerArrayfull(g.getPlayers())) {
			g.generateBots();
		}
		
		
		int countRound = 1;
	
		do {		
		CardDeck c = new CardDeck();
		ArrayList<Card> deck = new ArrayList<Card>(c.generateNumberCardList());
		Collections.shuffle(deck);
		g.setCardDeck(deck);
		g.dealCards(g.getPlayers());
		g.setCardStack(g.putFirstCardOnCardStack());

		g.setCurrentPlayer(g.whoStarts(g.getPlayers()));

		
		
		do {
			
			
			
			Card currentStackCard = g.getCardStack().get(g.getCardStack().size() - 1);

			System.out.println("Card on Stack: " + currentStackCard);
			System.out.println("Current Player: " + g.getCurrentPlayer().getName());
			System.out.println("My Cards: " + g.getCurrentPlayer().getCards());

			
			boolean validCard = false;
			while (validCard == false) {
				String action = g.getCurrentPlayer().selectAction(currentStackCard);
				if (action.equals("t")) {
					g.newCardDeck(); // pr�ft, ob noch Karten im Kartenstapel sind. Falls nicht, wird ein neuer Kartenstapel aus dem Ablagestapel generiert
					g.getCurrentPlayer().takeCard(g.getCardDeck().remove(0));
					validCard = true;
				} else if (action.equals("s")) {
					Card selectedCard = g.getCurrentPlayer().selectCard(currentStackCard);
					if (selectedCard.match(currentStackCard)) {
						g.getCurrentPlayer().removeCard(selectedCard);
						g.getCardStack().add(selectedCard);

						validCard = true;
					} else {
						System.out.println("Card doesn't match. Select another one or take a card from the stack.");

					}
				}
				if(g.howManyCardsAreThere(76) == false) {
					System.out.println("zu viele oder zu wenig Karten!!!");
					return;
				}
				if(!g.getCurrentPlayer().hasCard()) {
					break;
				}
				else {
				g.setCurrentPlayer(g.nextPlayer());
				}
		}
		
			}while (g.getCurrentPlayer().hasCard());
		System.out.println("Round over. " + g.getCurrentPlayer().getName() + " wins round " + countRound);
		
		countRound++;
		g.getCurrentPlayer().setPointsPerRound(g.addUpPointsPerRound());
		g.addUpTotalPointsForRoundWinner(g.getCurrentPlayer());
		g.emptyPlayerHands();
		g.emptyCardStack();
		
		for(int i = 0;i<g.getPlayers().length;i++) {
			System.out.println(g.getPlayers()[i].getName() + " has " + g.getPlayers()[i].getTotalPoints() + " points");
		}
		System.out.println("");
		
}while(!g.playerHas500PointsOrMore());
		
		System.out.println("Game over. " + g.whoWinsTheGame().getName() + " wins the game.");
}
}
