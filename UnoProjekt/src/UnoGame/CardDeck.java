package UnoGame;
import java.util.ArrayList;
import java.util.Collections;

public class CardDeck {

	
	// NumberCards generieren
	
	public ArrayList<NumberCard> generateNumberCardList(){
	ArrayList<NumberCard> numberCardList = new ArrayList<NumberCard>();
	for(int i = 0;i<10;i++)
	{
		int counter = 0;
		if (i == 0) {
			numberCardList.add(new NumberCard("red", i, "number"));
			numberCardList.add(new NumberCard("green", i, "number"));
			numberCardList.add(new NumberCard("blue", i, "number"));
			numberCardList.add(new NumberCard("yellow", i, "number"));
		} else {
			while (counter < 2) {
				numberCardList.add(new NumberCard("red", i, "number"));
				numberCardList.add(new NumberCard("green", i, "number"));
				numberCardList.add(new NumberCard("blue", i, "number"));
				numberCardList.add(new NumberCard("yellow", i, "number"));
				counter++;
			}
		}
	}
	return numberCardList;
	}

	// ActionCards generieren (pro Farbe je 2 mal TakeTwo, Skip und Reverse)
	//enorm umst�ndlich programmiert, aber kann man sp�ter noch �ndern


	public ArrayList<ActionCard> generateActionCardList(){
		 ArrayList<ActionCard> actionCardList = new ArrayList<ActionCard>();
	for(int j = 0; j<2; j++)
	{
		actionCardList.add(new ActionCard("red",20,"takeTwo"));
		actionCardList.add(new ActionCard("red",20,"skip"));
		actionCardList.add(new ActionCard("red",20,"reverse"));
		actionCardList.add(new ActionCard("blue",20,"takeTwo"));
		actionCardList.add(new ActionCard("blue",20,"skip"));
		actionCardList.add(new ActionCard("blue",20,"reverse"));
		actionCardList.add(new ActionCard("green",20,"takeTwo"));
		actionCardList.add(new ActionCard("green",20,"skip"));
		actionCardList.add(new ActionCard("green",20,"reverse"));
		actionCardList.add(new ActionCard("yellow",20,"takeTwo"));
		actionCardList.add(new ActionCard("yellow",20,"skip"));
		actionCardList.add(new ActionCard("yellow",20,"reverse"));		
	}
	return actionCardList;
	}
	
	//WildCards generieren (je 4 mal Farbwechsel und Farbwechsel+4)
	

	public ArrayList<WildCard> generateWildCardList(){
		ArrayList<WildCard> wildCardList = new ArrayList<WildCard>();
	for(int k = 0; k<4; k++) {
		wildCardList.add(new WildCard("none",50,"changeColor"));
		wildCardList.add(new WildCard("none",50,"changeColorTakeFour"));
	}
	return wildCardList;
	}
	
	//Kartenstapel generieren und auch gleich mischen
	

	public ArrayList<Card> getCardDeck() {
		ArrayList<Card> cardDeckList = new ArrayList<Card>();
		
		cardDeckList.addAll(generateNumberCardList());
		cardDeckList.addAll(generateActionCardList());
		cardDeckList.addAll(generateWildCardList());
		
		Collections.shuffle(cardDeckList);
		
		return cardDeckList;
	}
	
	

	}
	

