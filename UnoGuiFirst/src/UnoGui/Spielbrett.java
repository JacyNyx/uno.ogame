package UnoGui;

import java.awt.EventQueue;

import javax.swing.JFrame;
import java.awt.Color;
import javax.swing.JEditorPane;
import javax.swing.JButton;

public class Spielbrett
{

	private JFrame frmUno;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Spielbrett window = new Spielbrett();
					window.frmUno.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Spielbrett()
	{
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize()
	{
		frmUno = new JFrame();
		frmUno.getContentPane().setBackground(new Color(135, 206, 235));
		frmUno.setTitle("UNO");
		frmUno.setBounds(100, 100, 1074, 698);
		frmUno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmUno.getContentPane().setLayout(null);
		
		JEditorPane dtrpnPlayer = new JEditorPane();
		dtrpnPlayer.setBackground(new Color(224, 255, 255));
		dtrpnPlayer.setText("Player 1");
		dtrpnPlayer.setBounds(208, 29, 650, 158);
		frmUno.getContentPane().add(dtrpnPlayer);
		
		JEditorPane dtrpnPlayer_3 = new JEditorPane();
		dtrpnPlayer_3.setBackground(new Color(224, 255, 255));
		dtrpnPlayer_3.setText("Player 3");
		dtrpnPlayer_3.setBounds(208, 469, 650, 158);
		frmUno.getContentPane().add(dtrpnPlayer_3);
		
		JEditorPane dtrpnPlayer_1 = new JEditorPane();
		dtrpnPlayer_1.setBackground(new Color(224, 255, 255));
		dtrpnPlayer_1.setText("Player 2");
		dtrpnPlayer_1.setBounds(25, 29, 173, 598);
		frmUno.getContentPane().add(dtrpnPlayer_1);
		
		JEditorPane dtrpnKartenstapel = new JEditorPane();
		dtrpnKartenstapel.setText("Kartenstapel");
		dtrpnKartenstapel.setBounds(234, 198, 167, 260);
		frmUno.getContentPane().add(dtrpnKartenstapel);
		
		JEditorPane dtrpnGelegteKarte = new JEditorPane();
		dtrpnGelegteKarte.setText("gelegte Karten");
		dtrpnGelegteKarte.setBounds(440, 198, 167, 260);
		frmUno.getContentPane().add(dtrpnGelegteKarte);
		
		JButton btnNewButton = new JButton("UNO");
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		btnNewButton.setBounds(646, 350, 198, 54);
		frmUno.getContentPane().add(btnNewButton);
		
		JButton btnAbheben = new JButton("abheben");
		btnAbheben.setBackground(Color.LIGHT_GRAY);
		btnAbheben.setBounds(646, 242, 198, 54);
		frmUno.getContentPane().add(btnAbheben);
		
		JEditorPane dtrpnPlayer_2 = new JEditorPane();
		dtrpnPlayer_2.setBackground(new Color(224, 255, 255));
		dtrpnPlayer_2.setText("Player 4");
		dtrpnPlayer_2.setBounds(868, 29, 173, 598);
		frmUno.getContentPane().add(dtrpnPlayer_2);
	}

	public Color getFrmUnoContentPaneBackground() {
		return frmUno.getContentPane().getBackground();
	}
	public void setFrmUnoContentPaneBackground(Color background) {
		frmUno.getContentPane().setBackground(background);
	}
}
