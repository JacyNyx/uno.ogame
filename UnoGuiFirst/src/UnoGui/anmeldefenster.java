package UnoGui;


import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Font;
import javax.swing.DropMode;
import javax.swing.JLabel;
import javax.swing.UIManager;

public class anmeldefenster extends JFrame
{

		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;
		private JPanel contentPane;
		private JTextField txtBitteNamenEingeben;

		/**
		 * Launch the application.
		 */
		public static void main(String[] args)
		{
			EventQueue.invokeLater(new Runnable()
			{
				public void run()
				{
					try
					{
						anmeldefenster frame = new anmeldefenster();
						frame.setVisible(true);
					} catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			});
		}

		/**
		 * Create the frame.
		 */
		public anmeldefenster()
		{
			setTitle("Anmeldung");
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setBounds(100, 100, 353, 234);
			contentPane = new JPanel();
			contentPane.setBackground(new Color(135, 206, 235));
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			txtBitteNamenEingeben = new JTextField();
			txtBitteNamenEingeben.setFont(UIManager.getFont("Slider.font"));
			txtBitteNamenEingeben.setDropMode(DropMode.INSERT);
			txtBitteNamenEingeben.setToolTipText("");
			txtBitteNamenEingeben.setBackground(new Color(240, 255, 255));
			txtBitteNamenEingeben.setForeground(new Color(0, 0, 0));
			txtBitteNamenEingeben.setBounds(10, 65, 318, 36);
			contentPane.add(txtBitteNamenEingeben);
			txtBitteNamenEingeben.setColumns(10);
			
			JButton btnStart = new JButton("START");
			btnStart.setToolTipText("Start");
			btnStart.setBackground(Color.LIGHT_GRAY);
			btnStart.setFont(new Font("Arial Rounded MT Bold", Font.ITALIC, 18));
			btnStart.setForeground(new Color(0, 0, 0));
			btnStart.setBounds(10, 124, 114, 47);
			contentPane.add(btnStart);
			
			JLabel lblName = new JLabel("Spielername:");
			lblName.setFont(new Font("Arial Rounded MT Bold", Font.ITALIC, 16));
			lblName.setBounds(10, 11, 125, 53);
			contentPane.add(lblName);
		}
	}


