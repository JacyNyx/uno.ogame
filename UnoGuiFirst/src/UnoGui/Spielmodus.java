package UnoGui;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Spielmodus extends JFrame
{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args)
	{
		EventQueue.invokeLater(new Runnable()
		{
			public void run()
			{
				try
				{
					Spielmodus frame = new Spielmodus();
					frame.setVisible(true);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Spielmodus()
	{
		setTitle("Spielmodus");
		setForeground(new Color(60, 179, 113));
		setFont(new Font("Arial monospaced for SAP", Font.ITALIC, 13));
		setBackground(new Color(60, 179, 113));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 362, 176);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(135, 206, 235));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Singleplayer");
		btnNewButton.setForeground(Color.BLACK);
		btnNewButton.setBounds(192, 63, 115, 41);
		btnNewButton.setBackground(Color.LIGHT_GRAY);
		contentPane.add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Multiplayer");
		btnNewButton_1.setBounds(41, 63, 115, 41);
		btnNewButton_1.setBackground(Color.LIGHT_GRAY);
		btnNewButton_1.setToolTipText("");
		contentPane.add(btnNewButton_1);
		
		JLabel lblNewLabel = new JLabel("Spiele mit Freunden oder mit dem Computer");
		lblNewLabel.setBounds(33, 11, 286, 54);
		lblNewLabel.setBackground(new Color(216, 191, 216));
		lblNewLabel.setForeground(new Color(0, 0, 0));
		lblNewLabel.setFont(new Font("Arial Rounded MT Bold", Font.ITALIC, 13));
		contentPane.add(lblNewLabel);
		setFocusTraversalPolicy(new FocusTraversalOnArray(new Component[]{contentPane, btnNewButton, btnNewButton_1, lblNewLabel}));
	}
}
